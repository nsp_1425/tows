/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import javafx.animation.FadeTransition;
import javafx.scene.control.Label;
import javafx.util.Duration;

/**
 *
 * @author Sajeevan
 */
public class PopUp {
    
        public static void showError(String message, Label lbl) {
        lbl.setStyle(
                "-fx-font-size: 16px;"
                + "-fx-text-fill: #008000;"
                + "-fx-font-family: Arial Narrow;"
        );
        lbl.setText(message);

        FadeTransition transition = new FadeTransition(Duration.seconds(5), lbl);
        transition.setFromValue(2);
        transition.setToValue(0);
        transition.setCycleCount(1);
        transition.play();
    }
        
    public static void showInfo(String message, Label lbl) {
        
        lbl.setStyle(
                "-fx-font-size: 14px;"
                + "-fx-font-family: Arial Narrow;"
        );
        
        lbl.setText(message);
    }
    
}
