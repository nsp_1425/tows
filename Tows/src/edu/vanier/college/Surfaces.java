package edu.vanier.college;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;



/**
 *
 * @author Joakim Genereux
 */
public class Surfaces {
    
    private String name;
    private double uS;
    private double uK;
    private Image image;
    
    
    
    public Surfaces(String name, double uS, double uK, Image image) {
        this.name = name;
        this.uS = uS;
        this.uK = uK;
        this.image = image;
    }
   
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getuS() {
        return uS;
    }

    public void setuS(int uS) {
        this.uS = uS;
    }

    public double getuK() {
        return uK;
    }

    public void setuK(int uK) {
        this.uK = uK;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }


    
    

    
}
