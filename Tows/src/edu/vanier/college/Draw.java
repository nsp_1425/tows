/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import java.text.DecimalFormat;
import java.util.ArrayList;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Sajeevan
 */
public class Draw {
    
    public static void bot(ArrayList<Bot> botList, GraphicsContext gc) {
        DecimalFormat df2 = new DecimalFormat("#.##");
        
        for (Bot bot : botList) 
            if (bot.isSelected){ 
                bot.render(gc);
                gc.strokeText(String.valueOf(bot.force) + " N", bot.positionX + 60,  bot.positionY);
            }
                
    }
    
    public static void cart(Sprite ropeRight, Sprite ropeLeft, Sprite cart,GraphicsContext gc) {
        ropeRight.render(gc);
        ropeLeft.render(gc);
        cart.render(gc);  
        
        
    }
}
