/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import javafx.scene.image.Image;



/**
 *
 * @author Sajeevan
 */
public class Gravity {
    private String name;
    private double g;
    private Image image;
    
    
    public Gravity(String name, double g) {
        this.name = name;
        this.g = g;  
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        this.g = g;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    } 
}
