package edu.vanier.college;

import edu.vanier.Tows.FXMLMainpageController;
import static java.lang.Math.atan;
import static java.lang.Math.sqrt;
import javafx.event.ActionEvent;



/**
 *
 * @author Joakim Genereux
 */
public class InclineFormulas {
    
    //variables
    private double fN;
    private double fS;
    private double fg;
    private double fK;
    private double a;
    private double angle;
    private double netPull; 


    
    public double general_formula(double teta, double uK, double uS, double m, double pull_right, double pull_left, double gravity, boolean isSlected){

        //Real Mode
        if(isSlected)
        if(uK >= 0.3){

        }
        else{
            netPull *= sqrt(1/0.3*uK);
        }           
        if(isSlected){
            //Acceleration when there are no pulling Force
            if(pull_right == 0 && pull_left == 0){

            if(teta == 0){
                a= 0;
            }
            if(teta > 0 && teta >= atan(uS)){
                 fN = m * gravity * Math.cos(teta);
                 fK = uK * fN;
                 fg = m * gravity * Math.sin(teta);
                 a = (-fg + fK)/ m;
            }
            else if(teta < 0 && Math.abs(teta) >= atan(uS)){
                 fN = m * gravity * Math.cos(teta);
                 fK = uK * fN;
                 fg = m * gravity * Math.sin(Math.abs(teta));
                 a = (fg - fK)/ m;
            }  
            else{
                a = 0;
            }
        }
        //Acceleration when there are forces
        else if(pull_right > 0 || pull_left > 0){

            if (teta == 0){
                if(uK >= 0.3){
                    netPull = pull_right - pull_left;
                }
                else{
                    netPull = (pull_right - pull_left) * sqrt(1/0.3*uK);
                } 
                fN = m * gravity * Math.cos(0);
                fS = uS * fN;
                if (Math.abs(netPull) > Math.abs(fS)) {
                    fK = Math.abs(uK * fN);
                    fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                    fK = (netPull > 0)? -fK : fK;
                    a = (netPull + fK)/ m;
                } 
                else{
                    a = 0;
                }
            }
            if(teta > 0){
                if(uK >= 0.3){
                    netPull = pull_right - pull_left;
                }
                else{
                    netPull = (pull_right - pull_left) * sqrt(1/0.3*uK);
                } 
                fN = m * gravity * Math.cos(teta);
                fS = uS * fN;
                if(teta >= atan(uS)){
                    fK = Math.abs(uK * fN);
                    fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                    fK = (netPull > 0)? -fK : fK;
                    fg = m*gravity*Math.sin(teta); 
                    a = (-fg + netPull + fK)/ m;                 
                }
                else{
                    if(Math.abs(netPull) > Math.abs(fS)){
                        fK = Math.abs(uK * fN);
                        fK = (fK > netPull)? Math.abs(netPull) : fK;
                        fK = (netPull > 0)? -fK : fK;
                        fg = 0;
                        a = (-fg + netPull + fK)/ m;
                    }
                    else{
                        a = 0;
                    }
                }
            }
            if(teta < 0){
                if(uK >= 0.3){
                    netPull = pull_right - pull_left;
                }
                else{
                    netPull = (pull_right - pull_left) * sqrt(1/0.3*uK);
                } 
                fN = m * gravity * Math.cos(teta);
                fS = uS * fN;
                if(Math.abs(teta) >= atan(uS)){
                    fK = Math.abs(uK * fN);
                    fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                    fK = (netPull > 0)? -fK : fK;
                    fg = m * gravity * Math.sin(teta); 
                    a = (fg + netPull + fK)/ m;                 
                }
                else{
                    if(Math.abs(netPull) > Math.abs(fS)){
                        fK = Math.abs(uK * fN);
                        fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                        fK = (netPull > 0)? -fK : fK;
                        fg = 0;
                        a = (fg + netPull + fK)/ m;
                    }
                    else{
                        a = 0;
                    }
                }
            }     
        }    
        }
        else{//RealModeToggle is not selected
        //Acceleration when there are no pulling Force
        if(pull_right == 0 && pull_left == 0){

            if(teta == 0){
                a= 0;
            }
            if(teta > 0 && teta >= atan(uS)){
                 fN = m * gravity * Math.cos(teta);
                 fK = uK * fN;
                 fg = m * gravity * Math.sin(teta);
                 a = (-fg + fK)/ m;
            }
            else if(teta < 0 && Math.abs(teta) >= atan(uS)){
                 fN = m * gravity * Math.cos(teta);
                 fK = uK * fN;
                 fg = m * gravity * Math.sin(Math.abs(teta));
                 a = (fg - fK)/ m;
            }  
            else{
                a = 0;
            }
        }
        //Acceleration when there are forces
        else if(pull_right > 0 || pull_left > 0){

            if (teta == 0){
                netPull = pull_right - pull_left;
                fN = m * gravity * Math.cos(0);
                fS = uS * fN;
                if (Math.abs(netPull) > Math.abs(fS)) {
                    fK = Math.abs(uK * fN);
                    fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                    fK = (netPull > 0)? -fK : fK;
                    a = (netPull + fK)/ m;
                } 
                else{
                    a = 0;
                }
            }
            if(teta > 0){
                netPull = pull_right - pull_left;
                fN = m * gravity * Math.cos(teta);
                fS = uS * fN;
                if(teta >= atan(uS)){
                    fK = Math.abs(uK * fN);
                    fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                    fK = (netPull > 0)? -fK : fK;
                    fg = m*gravity*Math.sin(teta); 
                    a = (-fg + netPull + fK)/ m;                 
                }
                else{
                    if(Math.abs(netPull) > Math.abs(fS)){
                        fK = Math.abs(uK * fN);
                        fK = (fK > netPull)? Math.abs(netPull) : fK;
                        fK = (netPull > 0)? -fK : fK;
                        fg = 0;
                        a = (-fg + netPull + fK)/ m;
                    }
                    else{
                        a = 0;
                    }
                }
            }
            if(teta < 0){
                netPull = pull_right - pull_left;
                fN = m * gravity * Math.cos(teta);
                fS = uS * fN;
                if(Math.abs(teta) >= atan(uS)){
                    fK = Math.abs(uK * fN);
                    fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                    fK = (netPull > 0)? -fK : fK;
                    fg = m * gravity * Math.sin(teta); 
                    a = (fg + netPull + fK)/ m;                 
                }
                else{
                    if(Math.abs(netPull) > Math.abs(fS)){
                        fK = Math.abs(uK * fN);
                        fK = (Math.abs(fK) > Math.abs(netPull))? Math.abs(netPull) : fK;
                        fK = (netPull > 0)? -fK : fK;
                        fg = 0;
                        a = (fg + netPull + fK)/ m;
                    }
                    else{
                        a = 0;
                    }
                }
            }     
        }
        }
        return a;
    }
    
    public double getA() {
        return a;
    }
       
    public static void main(String[] args) {
        InclineFormulas f1 = new InclineFormulas();
        //(double teta, double uK, double uS, double m, double pull_pos, double pull_neg, double gravity)
        System.out.println(f1.general_formula(10, 0.7, 0.95, 20, 300, 0, -9.8, false));
    }
}