package edu.vanier.college;

import static java.lang.Math.atan;



/**
 *
 * @author Joakim Genereux
 */
public class Formulas {
    

    //user inputs
//    private double teta_pos;
//    private double teta_neg;
//    private double m;
//    private double uK;
//    private double pull_pos;
//    private double pull_neg;
//    private double angle;
    
    //variables
    private double fN;
    private double fS;
    private double fg;
    private double fK;
    private double a;
    private double angle;
    private double netPull; 
    
    //Constant
    private final double g = -3.0;

       public double general_formula(double teta_pos, double teta_neg, double uK, double uS, double m, double pull_pos, double pull_neg){
        //Acceleration when there are no pulling Force
        if(pull_pos == 0 && pull_neg == 0){
            /*if(teta_pos > 0 && teta_neg > 0){
                String slopeError = "Can only apply a slope on one side";
                return 0;
            }*/
            if(teta_pos == 0 && teta_neg == 0){
                a= 0;
            }
            if(teta_pos > 0 && teta_pos >= atan(uS)){
                 angle = teta_pos;
                 fN = m*g*Math.cos(angle);
                 fK = uK*fN;
                 fg = m*g*Math.sin(angle);
                 a = (-fg + fK)/m;
            }
            else if(teta_neg > 0 && teta_neg >= atan(uS)){
                 angle = teta_neg;
                 fN = m*g*Math.cos(angle);
                 fK = uK*fN;
                 fg = m*g*Math.sin(angle);
                 a = (fg - fK)/m;
            }  
            else{
                a = 0;
            }
        }
        //Acceleration when there are forces
        else if(pull_pos > 0 || pull_neg > 0){
            if(teta_pos > 0 && teta_neg > 0){
            String slopeError = "Can only apply a slope on one side";
            return 0;
            }
            if(teta_pos == 0 && teta_neg == 0){
                netPull = pull_pos - pull_neg;
                fN = m*g*Math.cos(0);
                fS = uS*fN;
                if(Math.abs(netPull) > Math.abs(fS)){
                fK = Math.abs(uK*fN);
                fK = (netPull > 0)? -fK : fK;
                a = (netPull + fK)/m;
                }
                else{
                a = 0;
                }
            }
            if(teta_pos > 0){
                angle = teta_pos;
                netPull = pull_pos - pull_neg;
                fN = m*g*Math.cos(angle);
                fS = uS*fN;
                if(teta_pos >= atan(uS)){
                        fK = Math.abs(uK*fN);
                        fg = m*g*Math.sin(angle); 
                        fK = (netPull > 0)? -fK : fK; 
                        a = (-fg + netPull + fK)/m;                  
                }
                else{
                    if(Math.abs(netPull) > Math.abs(fS)){
                        fK = Math.abs(uK*fN);
                        fg = 0;
                        fK = (netPull > 0)? -fK : fK; 
                        a = (-fg + netPull + fK)/m;
                    }
                    else{
                        a = 0;
                    }
                }
            }
            if(teta_neg > 0){
                angle = teta_neg;
                netPull = pull_pos - pull_neg;
                fN = m*g*Math.cos(angle);
                fS = uS*fN;
                if(teta_neg >= atan(uS)){
                        fK = Math.abs(uK*fN);
                        fg = m*g*Math.sin(angle); 
                        fK = (netPull > 0)? -fK : fK; 
                        a = (fg + netPull + fK)/m;                  
                }
                else{
                    if(Math.abs(netPull) > Math.abs(fS)){
                        fK = Math.abs(uK*fN);
                        fg = 0;
                        fK = (netPull > 0)? -fK : fK; 
                        a = (fg + netPull + fK)/m;
                    }
                    else{
                        a = 0;
                    }
                }
            }     
        }
        return a;
    }
       
       public static void main(String[] args) {
        Formulas f1 = new Formulas();
        f1.general_formula(0, 10, 1.00, 1.16, 60, 120, 60);
        System.out.println(f1.general_formula(0, 10, 1.00, 1.16, 60, 120, 60));
    }
}