/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Sajeevan
 */
public class Platforms {
    
    public static void rotation(double degree, Canvas canvas,  GraphicsContext gc) {
        gc.translate(canvas.getWidth() / 2, canvas.getHeight() / 2);
        gc.rotate(-degree);
        gc.translate(-canvas.getWidth() / 2, -canvas.getHeight() / 2);
    }
    
    public static void frictionGroundLeft(GraphicsContext gc, Image leftFrictionImg, Rectangle ground) {
        /*double xPoints[] = {ground.getBoundsInParent().getMinX() - 300, ground.getBoundsInParent().getMaxX()/2, ground.getBoundsInParent().getMaxX()/2, ground.getBoundsInParent().getMinX() - 300};
        double yPoints[] = {ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY() * 1.05,  ground.getBoundsInParent().getMinY() * 1.05};*/

        gc.drawImage(leftFrictionImg, ground.getBoundsInParent().getMinX() - 300, ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMaxX() / 2 - ground.getBoundsInParent().getMinX() + 300, ground.getBoundsInParent().getMinY() * 1.05 - ground.getBoundsInParent().getMinY());
    }

    public static void frictionGroundRight(GraphicsContext gc, Image rightFrictionImg, Rectangle ground) {
        /*double xPoints[] = {ground.getBoundsInParent().getMaxX()/2 - 1, ground.getBoundsInParent().getMaxX() + 300, ground.getBoundsInParent().getMaxX() + 300, ground.getBoundsInParent().getMaxX()/2 - 1};
        double yPoints[] = {ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY() * 1.05,  ground.getBoundsInParent().getMinY() * 1.05};*/

        gc.drawImage(rightFrictionImg, ground.getBoundsInParent().getMaxX() / 2 - 1, ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMaxX() + 300 - ground.getBoundsInParent().getMaxX() / 2, ground.getBoundsInParent().getMinY() * 1.05 - ground.getBoundsInParent().getMinY());
    }
    
        public static void ramp(GraphicsContext gc, Rectangle ground) {

        double xPoints[] = {ground.getBoundsInParent().getMinX() - 300, ground.getBoundsInParent().getMaxX() + 300, ground.getBoundsInParent().getMaxX() + 300, ground.getBoundsInParent().getMinX() - 300};
        double yPoints[] = {ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMaxY() + 300, ground.getBoundsInParent().getMaxY() + 300};

        gc.setFill(javafx.scene.paint.Color.BROWN);
        gc.fillPolygon(xPoints, yPoints, 4);

    }

    public static void end(GraphicsContext gc, Rectangle ground) {

        double xPointsL[] = {ground.getBoundsInParent().getMinX() + 100, ground.getBoundsInParent().getMinX() + 130, ground.getBoundsInParent().getMinX() + 130, ground.getBoundsInParent().getMinX() + 115};
        double yPointsL[] = {ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY() - 25, ground.getBoundsInParent().getMinY() - 25};

        double xPointsR[] = {ground.getBoundsInParent().getMaxX() - 100, ground.getBoundsInParent().getMaxX() - 130, ground.getBoundsInParent().getMaxX() - 130, ground.getBoundsInParent().getMaxX() - 115};
        double yPointsR[] = {ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY(), ground.getBoundsInParent().getMinY() - 25, ground.getBoundsInParent().getMinY() - 25};

        gc.setFill(javafx.scene.paint.Color.GRAY);
        gc.fillPolygon(xPointsL, yPointsL, 4);
        gc.fillPolygon(xPointsR, yPointsR, 4);

    }

}
