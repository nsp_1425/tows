/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import edu.vanier.Tows.FXMLMainpageController;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Sajeevan
 */
public class Bot extends Sprite {
    public String name;
    public boolean isSelected;
    public double force;

    
    public Bot(double force, String side, String name, double posX, double posY, boolean isSelected) {
        DecimalFormat df2 = new DecimalFormat("#.##");
        this.name = name;
        this.side = side;
        this.force = force;
        this.positionX = posX;
        this.positionY = posY;
        this.isSelected = isSelected;
        
        image = resizedImage(12);
    }

    @Override
    public String toString() {
        return side;
    }

    public boolean getIsSelected() {
        return isSelected;
    }
    
    

    
    public Image resizedImage(int scale){
     
            Image botImg = new Image("edu/vanier/images/" + name + ".png");
            BufferedImage inputImage = SwingFXUtils.fromFXImage(botImg, null);
            
            
            BufferedImage outputImage = new BufferedImage(154 + scale, 258 + scale, inputImage.getType());
            
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0,154 + scale,258 + scale, null);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.dispose();
            
            byte[] imageByte = null;
            try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            ImageIO.write(outputImage, "png", baos);

            baos.flush();
            imageByte = baos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainpageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        InputStream ip = new ByteArrayInputStream(imageByte);;
               
        return new Image(ip);
        
    }
    
}

