/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import java.util.ArrayList;
import javafx.scene.image.Image;

/**
 *
 * @author Sajeevan
 */
public class Values {

    public static double posPullForce(ArrayList<Bot> botList) {
        double rightForce = 0;

        for (Bot bot : botList) {
            if (bot.side.equals("Right")) {
                rightForce += bot.force;
            }
        }

        return rightForce;
    }

    public static double negPullForce(ArrayList<Bot> botList) {
        double leftForce = 0;

        for (Bot bot : botList) {
            if (bot.side.equals("Left")) {
                leftForce += bot.force;
            }
        }

        return leftForce;
    }

    public static double staticFrcition(String frictionName, ArrayList<Surfaces> surfaceList) {
        double uS = 0;

        for (Surfaces surface : surfaceList) {
            if (frictionName.equals(surface.getName())) {
                uS = surface.getuS();
            }
        }

        return uS;
    }

    public static Image frcitionImage(String frictionName, ArrayList<Surfaces> surfaceList) {
        Image img = null;

        for (Surfaces surface : surfaceList) {
            if (frictionName.equals(surface.getName())) {
                img = surface.getImage();
            }
        }

        return img;
    }

    public static double kineticFrcition(String frictionName, ArrayList<Surfaces> surfaceList) {
        double uK = 0;

        for (Surfaces surface : surfaceList) {
            if (frictionName.equals(surface.getName())) {
                uK = surface.getuK();
            }
        }

        return uK;
    }

    public static double gravity(String gravityName, ArrayList<Gravity> gravityList) {
        double g = 0;

        for (Gravity gravity : gravityList) {
            if (gravityName.equals(gravity.getName())) {
                g = gravity.getG();
            }
        }

        return g;
    }
    
    public static void botList(ArrayList<Bot> botList, double botLeftX, double botRightX,double botY){
        botList.add(new Bot(0, "Left", "botLeft1", botLeftX, botY, false));
        botList.add(new Bot(0, "Left", "botLeft2", botLeftX - 75 * 1, botY, false));
        botList.add(new Bot(0, "Left", "botLeft3", botLeftX - 75 * 2, botY, false));
        botList.add(new Bot(0, "Left", "botLeft4", botLeftX - 75 * 3, botY, false));
        botList.add(new Bot(0, "Right", "botRight1", botRightX, botY, false));
        botList.add(new Bot(0, "Right", "botRight2", botRightX + 75 * 1, botY, false));
        botList.add(new Bot(0, "Right", "botRight3", botRightX + 75 * 2, botY, false));
        botList.add(new Bot(0, "Right", "botRight4", botRightX + 75 * 3, botY, false));
    }
    
    public static void surfaceList(ArrayList<Surfaces> surfaceList){
        surfaceList.add(new Surfaces("rubber", 1.16, 1.00, new Image("edu/vanier/images/rubber.jpg")));
        surfaceList.add(new Surfaces("concrete", 1.05, 0.75, new Image("edu/vanier/images/concrete.jpg")));
        surfaceList.add(new Surfaces("Asphalt", 0.95, 0.70, new Image("edu/vanier/images/asphalt.jpg")));
        surfaceList.add(new Surfaces("steel", 0.5, 0.42, new Image("edu/vanier/images/steel.jpg")));
        surfaceList.add(new Surfaces("ice", 0.1, 0.05, new Image("edu/vanier/images/ice.jpg")));
        surfaceList.add(new Surfaces("wood", 0.6, 0.5, new Image("edu/vanier/images/wood.jpg")));
        surfaceList.add(new Surfaces("glass", 0.7, 0.6, new Image("edu/vanier/images/glass.jpg"))); 
    }
    
    public static void gravityList(ArrayList<Gravity> gravityList) {
        gravityList.add(new Gravity("Earth", 9.798));
        gravityList.add(new Gravity("Jupiter", 24.92));
        gravityList.add(new Gravity("Neptune", 11.15));
        gravityList.add(new Gravity("Saturn", 10.44));
        gravityList.add(new Gravity("Uranus", 8.87));
        gravityList.add(new Gravity("Venus", 8.87));
        gravityList.add(new Gravity("Mars", 3.71));
        gravityList.add(new Gravity("Mercury", 3.7));
        gravityList.add(new Gravity("Moon", 1.62));
        gravityList.add(new Gravity("Pluto", 0.58));   
    }

}
