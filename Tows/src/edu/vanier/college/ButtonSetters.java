/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.college;

import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleGroup;

/**
 *
 * @author Sajeevan
 */
public class ButtonSetters {
   
    public static void botAdd(ArrayList<Bot> botList, String side, ToggleGroup botGroup) {
        
        botGroup.getToggles().forEach(toggle -> {
            Node node = (Node) toggle;
            node.setDisable(false);

        });

        for (Bot bot : botList) {
            if (bot.side.equals(side)) {
                if (bot.isSelected) {
                    botGroup.getToggles().forEach(toggle -> {
                        if (toggle.getUserData().equals(bot.name)) {
                            Node node = (Node) toggle;
                            node.setDisable(true);

                        }
                    });
                }
            }
        }
        
        /*for (Bot bot : botList) {
            if (botGroup.getSelectedToggle().getUserData().equals(bot.name)) {

                botGroup.getSelectedToggle().setSelected(false); 
            }
        }*/
    }
    
    public static void cancelSelect(String name, ToggleGroup group){
        for (int i = 0; i < group.getToggles().size(); i++) {
            if (group.getToggles().get(i).getUserData().toString().equals(name)) {
                group.getSelectedToggle().setSelected(false);
            }
        }
    }
    
    public static void gravity(ArrayList<Bot> botList, ComboBox<String> cmbGravity){
        int counter = 0;

        for (Bot bot : botList) 
            if (bot.isSelected) 
                counter++;
           
        if (counter == 0) 
            cmbGravity.setDisable(false);
         else 
            cmbGravity.setDisable(true);        
    }
    
    public static void botRem(ArrayList<Bot> botList, String side, ToggleGroup botGroup) {

        botGroup.getToggles().forEach(toggle -> {
            Node node = (Node) toggle;
            node.setDisable(true);
        });

        for (Bot bot : botList) {
            if (bot.side.equals(side)) {
                if (bot.isSelected) {
                    botGroup.getToggles().forEach(toggle -> {
                        if (toggle.getUserData().equals(bot.name)) {
                            Node node = (Node) toggle;
                            node.setDisable(false);

                        }
                    });
                }
            }
        }
    }
    
}
