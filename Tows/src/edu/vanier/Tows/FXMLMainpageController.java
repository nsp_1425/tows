/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.vanier.Tows;

import edu.vanier.college.Bot;
import edu.vanier.college.ButtonSetters;
import edu.vanier.college.Draw;
import edu.vanier.college.Gravity;
import edu.vanier.college.InclineFormulas;

import edu.vanier.college.LongValue;
import edu.vanier.college.Platforms;
import edu.vanier.college.PopUp;
import edu.vanier.college.Sprite;
import edu.vanier.college.Surfaces;
import edu.vanier.college.Values;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Sajeevan
 */
public class FXMLMainpageController implements Initializable {
    
 
    private ArrayList<Bot> botList = new ArrayList<>();
    @FXML
    private Canvas canvas;
    private GraphicsContext gc;

    private Sprite ropeLeft = new Sprite();
    private Sprite ropeRight = new Sprite();
    private Sprite cart = new Sprite();
    
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    
    private ArrayList<Surfaces> surfaceList = new ArrayList<>();
    private ArrayList<Gravity> gravityList = new ArrayList<>();
    
    private double acc;
    
    private Image leftFrictionImg;
    private Image rightFrictionImg;

    @FXML private AnchorPane mainPane;
    @FXML private AnchorPane acrControls;
    private AnchorPane arRemove;
    
    @FXML private Rectangle ground;
    @FXML private Rectangle friction;

    @FXML private Button btnAnimation;
    @FXML private Button btnadd;
    @FXML  private javafx.scene.control.Button btnHelp;
    @FXML private javafx.scene.control.Button btnRefresh;
       
    @FXML private ComboBox<String> cmbSide;
    @FXML private ComboBox<String> cmbFriction;
    @FXML private ComboBox<String> cmbGravity;
    
    @FXML private TextField txtForce;
    @FXML private TextField txtAngle;
    
    @FXML private Slider sliderCartMass;
    @FXML private Slider sliderForce;
    @FXML private Slider sliderAngle;
    
    @FXML private Label lblAngleAlert;
    @FXML private Label lblFriction;
    @FXML private Label lblGravity;
    @FXML private Label lblForceAlert;
    @FXML private Label titleRemove;
    @FXML private Label subTitleRemove;
    
    @FXML
    private TextField txtCartMass;
    @FXML
    private Label lblCartMassAlert;
    @FXML
    private ToggleGroup botGroupAdd;
    @FXML
    private ToggleGroup botGroupRem;
    @FXML
    private RadioButton rbBot1Add;
    @FXML
    private RadioButton rbBot2Add;
    @FXML
    private RadioButton rbBot3Add;
    @FXML
    private RadioButton rbBot4Add;
    @FXML
    private RadioButton rbBot1Rem;
    @FXML
    private RadioButton rbBot2Rem;
    @FXML
    private RadioButton rbBot3Rem;
    @FXML
    private RadioButton rbBot4Rem;
    @FXML
    private Button btnRemove;

    @FXML
    private Button btnRandom;
    @FXML
    private RadioButton rdbRealisticMode;
    @FXML
    private Label lblWinner;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gc = canvas.getGraphicsContext2D();
        
        Image ropeImg = new Image("edu/vanier/images/rope.png");
        Image cartImg = new Image("edu/vanier/images/cart.png");
        
        cart.setImage(cartImg);
        cart.setPosition(cartStartX(), cartStartY());   
        
        ropeLeft.setImage(ropeImg);
        ropeLeft.setPosition(ropeLeftX(), ropeLeftY());

        ropeRight.setImage(ropeImg);
        ropeRight.setPosition(ropeRightX(), ropeRightY());

        
        
        Values.botList(botList, botLeftX(), botRightX(), botY());
        
        Values.surfaceList(surfaceList);
        
        Values.gravityList(gravityList);
        
        for(Surfaces surface : surfaceList)
            cmbFriction.getItems().add(surface.getName());      
        cmbFriction.setValue("Asphalt");
              
        for (Gravity gravity : gravityList) 
            cmbGravity.getItems().add(gravity.getName());              
        cmbGravity.setValue("Earth");
    
        cmbSide.getItems().addAll("Left", "Right");
        cmbSide.setValue("Left");
        
        txtCartMass.setText(String.valueOf(df2.format(getMassCart())));       
        txtForce.setText(String.valueOf(df2.format(getForce())));       
        txtAngle.setText(String.valueOf(df2.format(getAngle())));
               
        leftFrictionImg = new Image("edu/vanier/images/asphalt.jpg");
        rightFrictionImg = new Image("edu/vanier/images/asphalt.jpg");
        

        String messageFriction = "Static Friction: " + Values.staticFrcition(getFrictionName(), surfaceList) + "\n"
                + "Kinetic Friction: " + Values.kineticFrcition(getFrictionName(), surfaceList);
        PopUp.showInfo(messageFriction, lblFriction);
        
        String messageGravity = "g value: " + Values.gravity(getGarvityName(), gravityList);
        PopUp.showInfo(messageGravity, lblGravity);
        
        setrbBotsAddValue();
        setrbBotsRemValue();
        drawMethods();
    }
    
    private void initializeBotList(){
        for(Bot bot : botList){
            if(bot.name.equals("botLeft1"))
                bot.setPosition(botLeftX(), botY());
            if(bot.name.equals("botLeft2"))
                bot.setPosition(botLeftX() - 75 * 1, botY());
            if(bot.name.equals("botLeft3"))
                bot.setPosition(botLeftX()- 75 * 2, botY());
            if(bot.name.equals("botLeft4"))
                bot.setPosition(botLeftX()- 75 * 3, botY());
            if(bot.name.equals("botRight1"))
                bot.setPosition(botRightX(), botY());
            if(bot.name.equals("botRight2"))
                bot.setPosition(botRightX()+ 75 * 1, botY());
            if(bot.name.equals("botRight3"))
                bot.setPosition(botRightX()+ 75 * 2, botY());
            if(bot.name.equals("botRight4"))
                bot.setPosition(botRightX()+ 75 * 3, botY());
        }
    }
    @FXML
    private void moveCart(ActionEvent event) {
        LongValue lastNanoTime = new LongValue(System.nanoTime());
        acc = getAcceleration();
        
        AnimationTimer animation = new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                // calculate time since last update.
                double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
                lastNanoTime.value = currentNanoTime;
                
                if(btnAnimation.getText().equals("Stop")){
                    for(Bot bot: botList)
                        if(bot.isSelected)
                            bot.setVelocity(acc, 0);
                    
                    ropeLeft.setVelocity(acc, 0);                      
                    cart.setVelocity(acc, 0);                                   
                    ropeRight.setVelocity(acc, 0);
                }
                else{
                    for (Bot bot: botList) 
                         if(bot.isSelected)
                            bot.setVelocity(0, 0);
                    
                    ropeLeft.setVelocity(0, 0);
                    cart.setVelocity(0, 0);
                    ropeRight.setVelocity(0, 0);
                }
                
               if(cart.getPosX() <= ground.getBoundsInParent().getMinX() + 115){
                    for (Bot bot: botList) 
                         if(bot.isSelected)
                            bot.setVelocity(0, 0);
                    
                    ropeLeft.setVelocity(0, 0);
                    cart.setVelocity(0, 0);
                    ropeRight.setVelocity(0, 0);
                    lblWinner.setText("Blue Team Wins\npress refresh to restart");
                    lblWinner.setTextFill(Color.BLUE);
                    disable();
                    
               }
                
               if(cart.getPosX() + cart.getWidth() >= ground.getBoundsInParent().getMaxX() - 115){
                    for (Bot bot: botList) 
                         if(bot.isSelected)
                            bot.setVelocity(0, 0);
                    
                    ropeLeft.setVelocity(0, 0);
                    cart.setVelocity(0, 0);
                    ropeRight.setVelocity(0, 0);
                    lblWinner.setText("Yellow Team Wins\npress refresh to restart");
                    lblWinner.setTextFill(Color.YELLOW);
                    disable();
                }
               
                for (Bot bot : botList) 
                    if (bot.isSelected) 
                        bot.update(elapsedTime);
                
                ropeRight.update(elapsedTime);
                ropeLeft.update(elapsedTime);
                cart.update(elapsedTime); 
                
                drawMethods();
                   
            }
        };
        animation.start();
        
        
        btnAnimation.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(btnAnimation.getText().equals("Start")){
                     animation.start();
                    btnAnimation.setText("Stop");
                    return;
                }
                if (btnAnimation.getText().equals("Stop")) {
                    animation.stop();
                    btnAnimation.setText("Start");
                    return;                   
                }
            }
        });
    }
    @FXML
    private void handlebtnAddCharacterAction(javafx.event.ActionEvent event) {
        String name = "";
        try {
            if (botGroupAdd.getSelectedToggle().getUserData().equals("ll")) //Must Check
                System.out.println("");          
        } catch (Exception e) {
            PopUp.showError("Must choose a bot!", lblForceAlert);
            return;
        }
        
        for (Bot bot : botList) {
            if (botGroupAdd.getSelectedToggle().getUserData().equals(bot.name)) {               
                bot.isSelected = true;
                bot.force = Double.parseDouble(txtForce.getText());
                
                ButtonSetters.botAdd(botList, getSide(), botGroupAdd);
                ButtonSetters.botRem(botList, getSide(), botGroupRem);
                
                initializeBotList();
                Draw.bot(botList, gc);
                
                name = bot.name;
                
            }
        }  
        ButtonSetters.cancelSelect(name, botGroupAdd);
        ButtonSetters.gravity(botList, cmbGravity);  
    }
    
    @FXML
    private void handleSideChangeAction(ActionEvent event) {
        setrbBotsAddValue();
        setrbBotsRemValue();
        ButtonSetters.botAdd(botList, getSide(), botGroupAdd);
        ButtonSetters.botRem(botList, getSide(), botGroupRem);
        setImageFriction();
        
    }  
    public double getAngle(){ 
        return sliderAngle.getValue();
    }

    public double getAcceleration(){
        InclineFormulas f1 = new InclineFormulas();
        f1.general_formula(getAngle(), Values.kineticFrcition(getFrictionName(), surfaceList), Values.staticFrcition(getFrictionName(), surfaceList), getMassCart(), Values.posPullForce(botList), Values.negPullForce(botList), Values.gravity(getGarvityName(), gravityList), rdbRealisticMode.isSelected());
        return f1.getA();
    }
    //Coordinates
    
    private double cartStartX() {
        return (ground.getBoundsInParent().getMinX() + ground.getBoundsInParent().getMaxX()) / 2.00 - (cart.getWidth() / 2.00);
    }

    private double cartStartY() {
        return ground.getBoundsInParent().getMinY() - cart.getHeight() + cart.getHeight() / 20;
    }

    private double ropeLeftX() {
        return cartStartX() - ropeLeft.getWidth() + 10;
    }

    private double ropeLeftY() {
        return cartStartY();
    }

    private double ropeRightX() {
        return cartStartX() + cart.getWidth() - 8;
    }

    private double ropeRightY() {
        return ropeLeftY();
    }

    private double botLeftX() {
        return cart.getPosX() - 150;
    }

    private double botRightX() {
        return cart.getPosX() + 150;
    }

    public double botY() {
        return cartStartY() - 90;
    }
    
    public String getFrictionName() {
        return cmbFriction.getValue();
    }

    public String getGarvityName() {
        return cmbGravity.getValue();
    }

    public double getForce() {       
        return sliderForce.getValue();
        
    }

    public String getSide() {
        return cmbSide.getValue();
    }    
    
    public double getMassCart(){
       return sliderCartMass.getValue();
    }
   
    public void setrbBotsAddValue(){
        rbBot1Add.setUserData("bot" + getSide() + "1");
        rbBot2Add.setUserData("bot" + getSide() + "2");
        rbBot3Add.setUserData("bot" + getSide() + "3");
        rbBot4Add.setUserData("bot" + getSide() + "4");
    }
    
    public void setrbBotsRemValue() {
        rbBot1Rem.setUserData("bot" + getSide() + "1");
        rbBot2Rem.setUserData("bot" + getSide() + "2");
        rbBot3Rem.setUserData("bot" + getSide() + "3");
        rbBot4Rem.setUserData("bot" + getSide() + "4");
    }
    
    @FXML
    private void handleGravityAction(ActionEvent event) {
        ButtonSetters.gravity(botList, cmbGravity);
        
        String messageGravity = "g value: " + Values.gravity(getGarvityName(), gravityList);
        PopUp.showInfo(messageGravity, lblGravity);
             
    }
    
    @FXML
    private void forceUpdate(MouseEvent event) {
        txtForce.setText(String.valueOf(df2.format(getForce())));

    }

    @FXML
    private void sliderForceUpdate(ActionEvent event) {
        double num = 0;
        try {
            num = Double.parseDouble(txtForce.getText());
        } catch (NumberFormatException e) {
            PopUp.showError("Enter a number", lblForceAlert);
        }

        if (num < sliderForce.getMin() || num > sliderForce.getMax()) {
            PopUp.showError("Must be in range", lblForceAlert);
            return;
        }
        sliderForce.setValue(num);

    }

    @FXML
    private void AngleUpdate(MouseEvent event) {
        txtAngle.setText(String.valueOf(df2.format(getAngle())));
    }

    @FXML
    private void sliderAngleUpdate(ActionEvent event) {
        double num = 0;
        try {
            num = Double.parseDouble(txtAngle.getText());
        } catch (NumberFormatException e) {
            PopUp.showError("Enter a number", lblAngleAlert);
        }

        if (num < sliderAngle.getMin() || num > sliderAngle.getMax()) {
            PopUp.showError("Must be in range", lblAngleAlert);
            return;
        }
        sliderAngle.setValue(num);
    }  
    
    private void slopeDraw(){
        Timeline liveTimeline = new Timeline(new KeyFrame(Duration.millis(50), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                drawMethods();

            }
        }));
        liveTimeline.setCycleCount(Timeline.INDEFINITE);
        liveTimeline.play();
    }
    
    @FXML
    private void handleSlopeAction(MouseEvent event) {
        slopeDraw();
    }
    
    @FXML
    private void handleRefreshAction(javafx.event.ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("FXMLMainpage.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);

            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.showAndWait();
        } catch (IOException ex) {
        } catch (Exception ex) {
        }
    }
    
    @FXML
    private void handleHelpAction(javafx.event.ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLHelpMenu.fxml"));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();

            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException ex) {
        } catch (Exception ex) {
        }
    }

    @FXML
    private void handleFrictions(ActionEvent event) {
        String messageFriction = "Static Friction: " + Values.staticFrcition(getFrictionName(), surfaceList) + "\n"
                + "Kinetic Friction: " + Values.kineticFrcition(getFrictionName(), surfaceList) ;
        PopUp.showInfo(messageFriction, lblFriction);
        
        setImageFriction();
        Platforms.frictionGroundLeft(gc, leftFrictionImg, ground);
        Platforms.frictionGroundRight(gc, rightFrictionImg, ground);
    }
       
    @FXML
    private void CartMassUpdate(MouseEvent event) {
        txtCartMass.setText(String.valueOf(df2.format(getMassCart())));   
    }

    @FXML
    private void sliderCartMassUpdate(ActionEvent event) {
        double num = 0;
        try {
            num = Double.parseDouble(txtCartMass.getText());
        } catch (NumberFormatException e) {
            PopUp.showError("Enter a number", lblCartMassAlert);
            
        }

        if (num < sliderCartMass.getMin() || num > sliderCartMass.getMax()) {
            PopUp.showError("Must be in range", lblCartMassAlert);
            return;
        }
        sliderCartMass.setValue(num);
    }
    
    public void drawMethods(){
        cart.erase(gc, canvas.getWidth(), canvas.getHeight());
        
        gc.save();
        
        Platforms.rotation(getAngle(), canvas, gc);
        Platforms.ramp(gc, ground);
        Platforms.end(gc, ground);
        Draw.cart(ropeRight, ropeLeft, cart, gc);
        Draw.bot(botList, gc);
        Platforms.frictionGroundLeft(gc, leftFrictionImg, ground);
        Platforms.frictionGroundRight(gc, rightFrictionImg, ground);
        
        gc.restore();
    }
    
    public void setImageFriction(){
        if (getSide().equals("Right")) 
            rightFrictionImg = Values.frcitionImage(getFrictionName(), surfaceList);
        
        if (getSide().equals("Left")) 
            leftFrictionImg = Values.frcitionImage(getFrictionName(), surfaceList);
        
    }

    @FXML
    private void handlebtnRemoveCharacterAction(ActionEvent event) {
        
        String name = "";
        
        try {
            if (botGroupRem.getSelectedToggle().getUserData().equals("ll")) //Must Check
            {
                System.out.println("");
            }
        } catch (Exception e) {
            PopUp.showError("Must choose a bot!", lblForceAlert);
        }

        for (Bot bot : botList) {
            if (botGroupRem.getSelectedToggle().getUserData().equals(bot.name)) {
                bot.isSelected = false;
                name = bot.name;
                drawMethods();
                ButtonSetters.botAdd(botList, getSide(), botGroupAdd);
                ButtonSetters.botRem(botList, getSide(), botGroupRem);

                
            }
        }

        ButtonSetters.gravity(botList, cmbGravity);
        ButtonSetters.cancelSelect(name, botGroupRem);
    }
        


    @FXML
    private void handleRandomizeAction(ActionEvent event) {
        double rangeFric = (Math.random()*(((surfaceList.size()- 1)- 0)+1))+ 0;
        double rangeGrav = (Math.random()*(((gravityList.size()- 1)- 0)+1))+ 0;          
        
        cmbFriction.setValue(surfaceList.get((int)rangeFric).getName());
        cmbGravity.setValue(gravityList.get((int)rangeGrav).getName());
        
        sliderAngle.setValue((Math.random()*(((sliderAngle.getMax()- 1)- sliderAngle.getMin())+1))+ sliderAngle.getMin());
        txtAngle.setText(String.valueOf(df2.format(getAngle())));
        slopeDraw();
        
        sliderCartMass.setValue((Math.random()*(((sliderCartMass.getMax()- 1)- sliderCartMass.getMin())+1))+ sliderCartMass.getMin());
    }

    private void disable(){
        btnadd.setDisable(true);
        btnRandom.setDisable(true);
        btnRemove.setDisable(true);
        sliderAngle.setDisable(true);
        sliderCartMass.setDisable(true);
        sliderForce.setDisable(true);
        cmbFriction.setDisable(true);
    }
}
